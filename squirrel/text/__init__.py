# coding=utf-8
# Copyright 2021 The TensorFlow Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Text datasets."""

from squirrel.text.ag_news_subset import AGNewsSubset
from squirrel.text.anli import Anli
from squirrel.text.blimp import Blimp
from squirrel.text.bool_q import BoolQ
from squirrel.text.c4 import C4
from squirrel.text.cfq import CFQ
from squirrel.text.cfq import CFQConfig
from squirrel.text.civil_comments import CivilComments
from squirrel.text.clinc_oos import ClincOOS
from squirrel.text.cos_e import CosE
from squirrel.text.definite_pronoun_resolution import DefinitePronounResolution
from squirrel.text.docnli import DocNLI
from squirrel.text.dolphin_number_word import DolphinNumberWord
from squirrel.text.drop import Drop
from squirrel.text.eraser_multi_rc import EraserMultiRc
from squirrel.text.esnli import Esnli
from squirrel.text.gap import Gap
from squirrel.text.gem import Gem
from squirrel.text.glue import Glue
from squirrel.text.goemotions import Goemotions
from squirrel.text.gpt3 import Gpt3
from squirrel.text.hellaswag import Hellaswag
from squirrel.text.imdb import IMDBReviews
from squirrel.text.irc_disentanglement import IrcDisentanglement
from squirrel.text.lambada import Lambada
from squirrel.text.librispeech_lm import LibrispeechLm
from squirrel.text.lm1b import Lm1b
from squirrel.text.math_dataset import MathDataset
from squirrel.text.movie_rationales import MovieRationales
from squirrel.text.multi_nli import MultiNLI
from squirrel.text.multi_nli_mismatch import MultiNLIMismatch
from squirrel.text.openbookqa import Openbookqa
from squirrel.text.paws_wiki import PawsWiki
from squirrel.text.paws_x_wiki import PawsXWiki
from squirrel.text.pg19 import Pg19
from squirrel.text.piqa import PIQA
from squirrel.text.qa4mre import Qa4mre
from squirrel.text.quac import Quac
from squirrel.text.race import Race
from squirrel.text.reddit_disentanglement import RedditDisentanglement
from squirrel.text.salient_span_wikipedia import SalientSpanWikipedia
from squirrel.text.salient_span_wikipedia import SalientSpanWikipediaConfig
from squirrel.text.scan import Scan
from squirrel.text.scan import ScanConfig
from squirrel.text.schema_guided_dialogue import SchemaGuidedDialogue
from squirrel.text.scicite import Scicite
from squirrel.text.sentiment140 import Sentiment140
from squirrel.text.snli import Snli
from squirrel.text.star_cfq import StarCFQ
from squirrel.text.story_cloze import StoryCloze
from squirrel.text.super_glue import SuperGlue
from squirrel.text.tiny_shakespeare import TinyShakespeare
from squirrel.text.trec import Trec
from squirrel.text.wiki40b import Wiki40b
from squirrel.text.wikiann import Wikiann
from squirrel.text.wikipedia import Wikipedia
from squirrel.text.wikipedia_toxicity_subtypes import WikipediaToxicitySubtypes
from squirrel.text.winogrande import Winogrande
from squirrel.text.wordnet import Wordnet
from squirrel.text.wsc273 import Wsc273
from squirrel.text.xnli import Xnli
from squirrel.text.xtreme_pawsx import XtremePawsx
from squirrel.text.xtreme_xnli import XtremeXnli
from squirrel.text.yelp_polarity import YelpPolarityReviews
