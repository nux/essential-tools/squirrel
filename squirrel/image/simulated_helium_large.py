"""
Squirrel module of the balanced helium nanodroplets data from the 2015 experiment at the
FERMI FEL-1. The experimental details are described in
Phys. Rev. Lett. 121, 255301; Langbehn et al (2018). In addition to
the scattering data, the data contains labels for a supervised
machine learning task. These labels are subject of the publication
Phys. Rev. E 99, 063309; Zimmermann et al (2019) about the applicability
of neural networks within the domain of coherent diffraction imaging.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import h5py
import numpy as np
import tensorflow as tf

import squirrel.public_api as sq

_CITATION = """Use only for internal testing"""

_DESCRIPTION = """
Squirrel module of a large test dataset created using the pyScatman software.
The physical setting is that of Helium Nanodroplets irradiated at 19.eV.

1320550 simulated wide angle diffraction images using various SphericalHarmonics
3 Sets of l, m, c -> l up to 4, m up to 4, c using 10 values between 0.001 and 1
7 particle sizes in nm (200, 283, 367, 450, 533, 617, 700) for every l, m, c set...

Structure of the coefficients is: (l1, l2, l3, m1, m2, m3, c1, c2, c3, s).
All parameters are normalized between 0 and 1 with the rule:
l1: l1 / 4,
l2: l2 / 4,
l3: l3 / 4,
m1: m1 / 4,
m2: m2 / 4,
m3: m3 / 4,
c1: (c1 - 0.001) / 0.999,
c2: (c2 - 0.001) / 0.999,
c3: (c3 - 0.001) / 0.999,
s: (s - 200) / 500
See https://nux-group.gitlab.io/pyscatman/shapes.html for an explanation on 
these parameters. 
"""

_URL = "https://share.phys.ethz.ch/~nux/datasets/large_simulated_helium_test_dataset.h5"

_IMG_SHAPE = (196, 196)

_COEF_SHAPE = (10,)

_IMAGE_FEATURE_DESCRIPTION = {
    "image": tf.io.FixedLenFeature([], tf.string),
    "coefficients": tf.io.FixedLenFeature([], tf.string),
}


class SimulatedHeliumNanodropletsLarge(sq.core.GeneratorBasedBuilder):
    """
    Squirrel module of a small test dataset created using the pyScatman software.
    50000 simulated wide angle diffraction images using various SphericalHarmonics
    Parameter l, m, and c.
    """

    VERSION = sq.core.Version("0.1.0")

    def _info(self):
        return sq.core.DatasetInfo(
            builder=self,
            # This is the description that will appear on the datasets page.
            description=_DESCRIPTION,
            # sq.features.FeatureConnectors
            features=sq.features.FeaturesDict({
                "image":
                    sq.features.Tensor(dtype=tf.float32,
                                       shape=_IMG_SHAPE),
                "coefficients":
                    sq.features.Tensor(dtype=tf.float64,
                                       shape=_COEF_SHAPE),
            }),
            # If there"s a common (input, target) tuple from the features,
            # specify them here. They"ll be used if as_supervised=True in
            # builder.as_dataset.
            supervised_keys=("image", "coefficients"),
            # Bibtex citation for the dataset
            citation=_CITATION,
        )

    def _split_generators(self, dl_manager):
        """Returns SplitGenerators."""
        # Download source data
        dl_path = dl_manager.download(_URL)

        # gen_kwargs will be passed as arguments to _generate_examples
        # Users should dynamically create their own subsplits
        # with the subsplit API (e.g. split="train[80%:]")
        # See: https://github.com/tensorflow/datasets/blob/master/docs/splits.md
        return [
            sq.core.SplitGenerator(
                name=sq.Split.TRAIN,
                gen_kwargs={
                    "path": dl_path,
                },
            )
        ]

    def _generate_examples(self, path):
        """Yields examples."""
        with h5py.File(path, "r") as ff:
            for ii, k in enumerate(ff):
                if k != "Parameters":
                    record = {"image": np.array(ff[k]["Image"]),
                              "coefficients": np.array(ff[k]["Label"]).astype(float)}
                    yield ii, record
