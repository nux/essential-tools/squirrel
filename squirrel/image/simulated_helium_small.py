"""
Squirrel module of the balanced helium nanodroplets data from the 2015 experiment at the
FERMI FEL-1. The experimental details are described in
Phys. Rev. Lett. 121, 255301; Langbehn et al (2018). In addition to
the scattering data, the data contains labels for a supervised
machine learning task. These labels are subject of the publication
Phys. Rev. E 99, 063309; Zimmermann et al (2019) about the applicability
of neural networks within the domain of coherent diffraction imaging.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import scipy.constants as cs
import tensorflow as tf

import squirrel.public_api as sq

_CITATION = """Use only for internal testing"""

_DESCRIPTION = """
Squirrel module of a small test dataset created using the pyScatman software.
54000 simulated wide angle diffraction images using various SphericalHarmonics
Parameters l, m, and particle sizes a, as well as different orientations lat and lon.

Structure of the coefficients is: (l, m, a, lat, lon).
See https://nux-group.gitlab.io/pyscatman/shapes.html for an explanation on 
these parameters. 
"""

_URL = "https://share.phys.ethz.ch/~nux/datasets/small_simulated_helium_test_dataset.npz"

_IMG_SHAPE = (196, 196)

_COEF_SHAPE = (5,)

_IMAGE_FEATURE_DESCRIPTION = {
    "image": tf.io.FixedLenFeature([], tf.string),
    "coefficients": tf.io.FixedLenFeature([], tf.string),
}


class SimulatedHeliumNanodropletsSmall(sq.core.GeneratorBasedBuilder):
    """
    Squirrel module of a small test dataset created using the pyScatman software.
    50000 simulated wide angle diffraction images using various SphericalHarmonics
    Parameter l, m, and c.
    """

    VERSION = sq.core.Version("0.1.0")

    def _info(self):
        return sq.core.DatasetInfo(
            builder=self,
            # This is the description that will appear on the datasets page.
            description=_DESCRIPTION,
            # sq.features.FeatureConnectors
            features=sq.features.FeaturesDict({
                "image":
                    sq.features.Tensor(dtype=tf.float32,
                                       shape=_IMG_SHAPE),
                "coefficients":
                    sq.features.Tensor(dtype=tf.float64,
                                       shape=_COEF_SHAPE),
            }),
            # If there"s a common (input, target) tuple from the features,
            # specify them here. They"ll be used if as_supervised=True in
            # builder.as_dataset.
            supervised_keys=("image", "coefficients"),
            # Bibtex citation for the dataset
            citation=_CITATION,
        )

    def _split_generators(self, dl_manager):
        """Returns SplitGenerators."""
        # Download source data
        dl_path = dl_manager.download(_URL)

        # gen_kwargs will be passed as arguments to _generate_examples
        # Users should dynamically create their own subsplits
        # with the subsplit API (e.g. split="train[80%:]")
        # See: https://github.com/tensorflow/datasets/blob/master/docs/splits.md
        return [
            sq.core.SplitGenerator(
                name=sq.Split.TRAIN,
                gen_kwargs={
                    "path": dl_path,
                },
            )
        ]

    def _generate_examples(self, path):
        """Yields examples."""
        data = np.load(path)
        keys = list(data.keys())
        for ii, (i, l) in enumerate(zip(data[keys[0]], data[keys[1]])):
            try:
                assert i.shape == _IMG_SHAPE
            except AssertionError as e:
                print(e, """\n Assertion of image shape failed. Provided by the routine: {}
                , but extracted from file: {}""".format(_IMG_SHAPE, i.shape))
                raise e

            record = {"image": i, "coefficients": l}
            yield ii, record
