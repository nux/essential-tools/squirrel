# coding=utf-8
# Copyright 2021 The TensorFlow Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Image datasets."""

from squirrel.image.abstract_reasoning import AbstractReasoning
from squirrel.image.aflw2k3d import Aflw2k3d
from squirrel.image.arc import ARC
from squirrel.image.bccd import BCCD
from squirrel.image.binarized_mnist import BinarizedMNIST
from squirrel.image.celeba import CelebA
from squirrel.image.celebahq import CelebAHq
from squirrel.image.cityscapes import Cityscapes
from squirrel.image.clevr import CLEVR
from squirrel.image.clic import CLIC
from squirrel.image.coil100 import Coil100
from squirrel.image.div2k import Div2k
from squirrel.image.downsampled_imagenet import DownsampledImagenet
from squirrel.image.dsprites import Dsprites
from squirrel.image.duke_ultrasound import DukeUltrasound
from squirrel.image.flic import Flic
from squirrel.image.lost_and_found import LostAndFound
from squirrel.image.lsun import Lsun
from squirrel.image.nyu_depth_v2 import NyuDepthV2
from squirrel.image.pass_dataset import PASS
from squirrel.image.s3o4d import S3o4d
from squirrel.image.scene_parse_150 import SceneParse150
from squirrel.image.shapes3d import Shapes3d
from squirrel.image.symmetric_solids import SymmetricSolids
from squirrel.image.the300w_lp import The300wLp
from squirrel.image.simulated_helium_large import SimulatedHeliumNanodropletsLarge
from squirrel.image_classification.beans import Beans
from squirrel.image_classification.bigearthnet import Bigearthnet
from squirrel.image_classification.binary_alpha_digits import BinaryAlphaDigits
from squirrel.image_classification.caltech import Caltech101
from squirrel.image_classification.caltech_birds import CaltechBirds2010
from squirrel.image_classification.cars196 import Cars196
from squirrel.image_classification.cassava import Cassava
from squirrel.image_classification.cats_vs_dogs import CatsVsDogs
from squirrel.image_classification.cbis_ddsm import CuratedBreastImagingDDSM
from squirrel.image_classification.chexpert import Chexpert
from squirrel.image_classification.cifar import Cifar10
from squirrel.image_classification.cifar import Cifar100
from squirrel.image_classification.cifar10_1 import Cifar10_1
from squirrel.image_classification.cifar10_corrupted import Cifar10Corrupted
from squirrel.image_classification.citrus import CitrusLeaves
from squirrel.image_classification.cmaterdb import Cmaterdb
from squirrel.image_classification.colorectal_histology import ColorectalHistology
from squirrel.image_classification.colorectal_histology import ColorectalHistologyLarge
from squirrel.image_classification.cycle_gan import CycleGAN
from squirrel.image_classification.deep_weeds import DeepWeeds
from squirrel.image_classification.diabetic_retinopathy_detection import DiabeticRetinopathyDetection
from squirrel.image_classification.dmlab import Dmlab
from squirrel.image_classification.dtd import Dtd
from squirrel.image_classification.eurosat import Eurosat
from squirrel.image_classification.flowers import TFFlowers
from squirrel.image_classification.food101 import Food101
from squirrel.image_classification.geirhos_conflict_stimuli import GeirhosConflictStimuli
from squirrel.image_classification.horses_or_humans import HorsesOrHumans
from squirrel.image_classification.imagenet import Imagenet2012
from squirrel.image_classification.imagenet2012_corrupted import Imagenet2012Corrupted
from squirrel.image_classification.imagenet2012_subset import Imagenet2012Subset
from squirrel.image_classification.imagenet_resized import ImagenetResized
from squirrel.image_classification.imagenette import Imagenette
from squirrel.image_classification.imagewang import Imagewang
from squirrel.image_classification.inaturalist import INaturalist2017
from squirrel.image_classification.lfw import LFW
from squirrel.image_classification.malaria import Malaria
from squirrel.image_classification.mnist import EMNIST
from squirrel.image_classification.mnist import FashionMNIST
from squirrel.image_classification.mnist import KMNIST
from squirrel.image_classification.mnist import MNIST
from squirrel.image_classification.mnist_corrupted import MNISTCorrupted
from squirrel.image_classification.omniglot import Omniglot
from squirrel.image_classification.oxford_flowers102 import OxfordFlowers102
from squirrel.image_classification.oxford_iiit_pet import OxfordIIITPet
from squirrel.image_classification.patch_camelyon import PatchCamelyon
from squirrel.image_classification.pet_finder import PetFinder
from squirrel.image_classification.places365_small import Places365Small
from squirrel.image_classification.plant_leaves import PlantLeaves
from squirrel.image_classification.plant_village import PlantVillage
from squirrel.image_classification.plantae_k import PlantaeK
from squirrel.image_classification.quickdraw import QuickdrawBitmap
from squirrel.image_classification.resisc45 import Resisc45
from squirrel.image_classification.rock_paper_scissors import RockPaperScissors
from squirrel.image_classification.smallnorb import Smallnorb
from squirrel.image_classification.so2sat import So2sat
from squirrel.image_classification.stanford_dogs import StanfordDogs
from squirrel.image_classification.stanford_online_products import StanfordOnlineProducts
from squirrel.image_classification.sun import Sun397
from squirrel.image_classification.svhn import SvhnCropped
from squirrel.image_classification.uc_merced import UcMerced
from squirrel.image_classification.visual_domain_decathlon import VisualDomainDecathlon
from squirrel.image_classification.balanced_static_helium import BalancedStaticHeliumNanodroplets
from squirrel.image_classification.static_helium import StaticHeliumNanodroplets
from squirrel.image_classification.balanced_static_helium_no_round import BalancedStaticHeliumNanodropletsNoRound
