# coding=utf-8
# Copyright 2021 The TensorFlow Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Translation datasets."""

from squirrel.translate.flores import Flores
from squirrel.translate.flores import FloresConfig
from squirrel.translate.opus import Opus
from squirrel.translate.para_crawl import ParaCrawl
from squirrel.translate.para_crawl import ParaCrawlConfig
from squirrel.translate.ted_hrlr import TedHrlrTranslate
from squirrel.translate.ted_multi import TedMultiTranslate
from squirrel.translate.wmt import WmtConfig
from squirrel.translate.wmt13 import Wmt13Translate
from squirrel.translate.wmt14 import Wmt14Translate
from squirrel.translate.wmt15 import Wmt15Translate
from squirrel.translate.wmt16 import Wmt16Translate
from squirrel.translate.wmt17 import Wmt17Translate
from squirrel.translate.wmt18 import Wmt18Translate
from squirrel.translate.wmt19 import Wmt19Translate
from squirrel.translate.wmt_t2t import WmtT2tTranslate
