# coding=utf-8
# Copyright 2021 The TensorFlow Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Text datasets."""

from squirrel.summarization.aeslc import Aeslc
from squirrel.summarization.big_patent import BigPatent
from squirrel.summarization.billsum import Billsum
from squirrel.summarization.booksum import Booksum
from squirrel.summarization.cnn_dailymail import CnnDailymail
from squirrel.summarization.covid19sum import Covid19sum
from squirrel.summarization.gigaword import Gigaword
from squirrel.summarization.gov_report import GovReport
from squirrel.summarization.multi_news import MultiNews
from squirrel.summarization.newsroom import Newsroom
from squirrel.summarization.opinion_abstracts import OpinionAbstracts
from squirrel.summarization.opinosis import Opinosis
from squirrel.summarization.reddit import Reddit
from squirrel.summarization.reddit_tifu import RedditTifu
from squirrel.summarization.samsum import Samsum
from squirrel.summarization.scientific_papers import ScientificPapers
from squirrel.summarization.summscreen import Summscreen
from squirrel.summarization.wikihow import Wikihow
from squirrel.summarization.xsum import Xsum
