# coding=utf-8
# Copyright 2021 The TensorFlow Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Public API of tfds, without the registered dataset."""

# pylint: disable=unused-import,g-import-not-at-top,g-bad-import-order,wrong-import-position
from squirrel.core import tf_compat

tf_compat.ensure_tf_install()

from squirrel import core
from squirrel import typing
from squirrel.core import folder_dataset
from squirrel.core import beam_utils as beam
from squirrel.core import download
from squirrel.core import decode
from squirrel.core import deprecated
from squirrel.core import features
from squirrel.core import visualization
from squirrel.core.as_dataframe import as_dataframe
from squirrel.core.folder_dataset import ImageFolder
from squirrel.core.folder_dataset import TranslateFolder
from squirrel.core.dataset_utils import as_numpy
from squirrel.core.download import GenerateMode
from squirrel.core.load import builder
from squirrel.core.load import builder_cls
from squirrel.core.load import list_builders
from squirrel.core.load import load
from squirrel.core.splits import Split
from squirrel.core.subsplits_utils import even_splits
from squirrel.core.utils.benchmark import benchmark
from squirrel.core.utils.gcs_utils import is_dataset_on_gcs
from squirrel.core.utils.read_config import ReadConfig
from squirrel.core.utils.tqdm_utils import disable_progress_bar
from squirrel.core.utils.tqdm_utils import enable_progress_bar
from squirrel.core.utils.tqdm_utils import display_progress_bar
from squirrel.core.visualization import show_examples
from squirrel.core.visualization import show_statistics
from squirrel.version import __version__

deprecated = core.utils.docs.deprecated(deprecated)

with core.registered.skip_registration():
  # We import testing namespace but without registering the tests datasets
  # (e.g. DummyMnist,...).
  from squirrel import testing

__all__ = [
    "as_dataframe",
    "as_numpy",
    "beam",
    "benchmark",
    "core",
    "deprecated",
    "folder_dataset",
    "builder",
    "builder_cls",
    "decode",
    "disable_progress_bar",
    "enable_progress_bar",
    "display_progress_bar",
    "download",
    "even_splits",
    "features",
    "GenerateMode",
    "ImageFolder",
    "is_dataset_on_gcs",
    "list_builders",
    "load",
    "ReadConfig",
    "Split",
    "show_examples",
    "show_statistics",
    "testing",
    "TranslateFolder",
    "typing",
    "visualization",
    "__version__",
]
