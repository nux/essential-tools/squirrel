# coding=utf-8
# Copyright 2021 The TensorFlow Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Audio datasets."""

from squirrel.audio.accentdb import Accentdb
from squirrel.audio.commonvoice import CommonVoice
from squirrel.audio.commonvoice import CommonVoiceConfig
from squirrel.audio.crema_d import CremaD
from squirrel.audio.dementiabank import Dementiabank
from squirrel.audio.fuss import Fuss
from squirrel.audio.groove import Groove
from squirrel.audio.gtzan import GTZAN
from squirrel.audio.gtzan_music_speech import GTZANMusicSpeech
from squirrel.audio.librispeech import Librispeech
from squirrel.audio.libritts import Libritts
from squirrel.audio.ljspeech import Ljspeech
from squirrel.audio.nsynth import Nsynth
from squirrel.audio.savee import Savee
from squirrel.audio.speech_commands import SpeechCommands
from squirrel.audio.spoken_digit import SpokenDigit
from squirrel.audio.tedlium import Tedlium
from squirrel.audio.vctk import Vctk
from squirrel.audio.voxceleb import Voxceleb
from squirrel.audio.voxforge import Voxforge
from squirrel.audio.yesno import YesNo
