# coding=utf-8
# Copyright 2021 The TensorFlow Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Question answering datasets."""

from squirrel.question_answering.ai2_arc import Ai2Arc
from squirrel.question_answering.ai2_arc_with_ir import Ai2ArcWithIR
from squirrel.question_answering.coqa import Coqa
from squirrel.question_answering.cosmos_qa import CosmosQA
from squirrel.question_answering.mctaco import Mctaco
from squirrel.question_answering.mlqa import Mlqa
from squirrel.question_answering.natural_questions import NaturalQuestions
from squirrel.question_answering.natural_questions_open import NaturalQuestionsOpen
from squirrel.question_answering.qasc import Qasc
from squirrel.question_answering.squad import Squad
from squirrel.question_answering.trivia_qa import TriviaQA
from squirrel.question_answering.tydi_qa import TydiQA
from squirrel.question_answering.web_questions import WebQuestions
from squirrel.question_answering.xquad import Xquad
