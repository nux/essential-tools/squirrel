# coding=utf-8
# Copyright 2021 The TensorFlow Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""API defining dataset features (image, text, scalar,...).

See [the guide](https://www.tensorflow.org/datasets/features).

"""

from squirrel.core.features.audio_feature import Audio
from squirrel.core.features.bounding_boxes import BBox
from squirrel.core.features.bounding_boxes import BBoxFeature
from squirrel.core.features.class_label_feature import ClassLabel
from squirrel.core.features.dataset_feature import Dataset
from squirrel.core.features.feature import FeatureConnector
from squirrel.core.features.feature import TensorInfo
from squirrel.core.features.features_dict import FeaturesDict
from squirrel.core.features.image_feature import Image
from squirrel.core.features.labeled_image import LabeledImage
from squirrel.core.features.sequence_feature import Sequence
from squirrel.core.features.tensor_feature import Encoding
from squirrel.core.features.tensor_feature import Tensor
from squirrel.core.features.text_feature import Text
from squirrel.core.features.translation_feature import Translation
from squirrel.core.features.translation_feature import TranslationVariableLanguages
from squirrel.core.features.video_feature import Video

__all__ = [
    "Audio",
    "BBox",
    "BBoxFeature",
    "ClassLabel",
    "Dataset",
    "Encoding",
    "FeatureConnector",
    "FeaturesDict",
    "LabeledImage",
    "Tensor",
    "TensorInfo",
    "Sequence",
    "Image",
    "Text",
    "Video",
]
