# coding=utf-8
# Copyright 2021 The TensorFlow Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Object detection datasets."""

from squirrel.object_detection.coco import Coco
from squirrel.object_detection.coco_captions import CocoCaptions
from squirrel.object_detection.kitti import Kitti
from squirrel.object_detection.lvis import Lvis
from squirrel.object_detection.open_images import OpenImagesV4
from squirrel.object_detection.open_images_challenge2019 import OpenImagesChallenge2019Detection
from squirrel.object_detection.voc import Voc
from squirrel.object_detection.waymo_open_dataset import WaymoOpenDataset
from squirrel.object_detection.wider_face import WiderFace
