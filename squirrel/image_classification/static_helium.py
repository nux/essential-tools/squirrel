"""
Squirrel module of the helium nanodroplets data from the 2015 experiment at the
FERMI FEL-1. The experimental details are described in
Phys. Rev. Lett. 121, 255301; Langbehn et al (2018). In addition to
the scattering data, the data contains labels for a supervised
machine learning task. These labels are subject of the publication
Phys. Rev. E 99, 063309; Zimmermann et al (2019) about the applicability
of neural networks within the domain of coherent diffraction imaging.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import scipy.constants as cs
import tensorflow as tf

import squirrel.public_api as sq

_CITATION = r"""@article{PhysRevE.99.063309,
title = {Deep neural networks for classifying complex features in diffraction images},
author = {Zimmermann, Julian and Langbehn, Bruno and Cucini, Riccardo and Di Fraia, Michele and Finetti, Paola and LaForge, Aaron C. and Nishiyama, Toshiyuki and Ovcharenko, Yevheniy and Piseri, Paolo and Plekan, Oksana and Prince, Kevin C. and Stienkemeier, Frank and Ueda, Kiyoshi and Callegari, Carlo and M\"oller, Thomas and Rupp, Daniela},
journal = {Phys. Rev. E},
volume = {99},
issue = {6},
pages = {063309},
numpages = {20},
year = {2019},
month = {Jun},
publisher = {American Physical Society},
doi = {10.1103/PhysRevE.99.063309},
url = {https://link.aps.org/doi/10.1103/PhysRevE.99.063309}
}
"""

_DESCRIPTION = """
Squirrel module of the static helium data from the 2015 experiment at the LDM
end station at FERMI FEL-1. The experimental details are described in
Phys. Rev. Lett. 121, 255301; Langbehn et al (2018). In addition to
the scattering data, the data file contains labels for a supervised
machine learning task.

The classes are:
0: Spherical / Oblate
1: Round
2: Elliptical
3: Prolate
4: Streak
5: Bent,
6: Asymmetric
7: NewtonRings
8: DoubleRings
9: Layered
10: Empty
"""

_URL = "https://share.phys.ethz.ch/~nux/datasets/helium_nanodroplets_with_labels_zimmermann.cxi"

_CLASS_NAMES = [
    "Spherical_Oblate", "Round", "Elliptical", "Prolate", "Streak", "Bent",
    "Asymmetric", "NewtonRings", "DoubleRings", "Layered", "Empty"
]

_IMG_SHAPE = (1035, 1035)
_EV = cs.physical_constants["electron volt"][0]


class StaticHeliumNanodroplets(sq.core.GeneratorBasedBuilder):
    """
    Squirrel module of the helium nanodroplets data from the 2015 experiment at the
    FERMI FEL-1. The experimental details are described in
    Phys. Rev. Lett. 121, 255301; Langbehn et al (2018). In addition to
    the scattering data, the data contains labels for a supervised
    machine learning task. These labels are subject of the publication
    Phys. Rev. E 99, 063309; Zimmermann et al (2019) about the applicability
    of neural networks within the domain of coherent diffraction imaging.
    """

    VERSION = sq.core.Version("0.1.1")

    def _info(self):
        return sq.core.DatasetInfo(
            builder=self,
            # This is the description that will appear on the datasets page.
            description=_DESCRIPTION,
            # sq.features.FeatureConnectors
            features=sq.features.FeaturesDict({
                "photon_energy":
                    sq.features.Tensor(dtype=tf.float32,
                                       shape=[]),
                "wavelength":
                    sq.features.Tensor(dtype=tf.float32,
                                       shape=[]),
                "detector_distance":
                    sq.features.Tensor(dtype=tf.float32,
                                       shape=[]),
                "bunch_id":
                    sq.features.Tensor(dtype=tf.int64,
                                       shape=[]),
                "image":
                    sq.features.Tensor(dtype=tf.uint16,
                                       shape=_IMG_SHAPE),
                "label":
                    sq.features.Tensor(dtype=tf.int64,
                                       shape=[len(_CLASS_NAMES)]),
            }),
            # If there"s a common (input, target) tuple from the features,
            # specify them here. They"ll be used if as_supervised=True in
            # builder.as_dataset.
            supervised_keys=("image", "label"),
            # Homepage of the dataset for documentation
            homepage="https://www.cxidb.org/id-94.html",
            # Bibtex citation for the dataset
            citation=_CITATION,
        )

    def _split_generators(self, dl_manager):
        """Returns SplitGenerators."""
        # Download source data
        dl_path = dl_manager.download(_URL)

        # gen_kwargs will be passed as arguments to _generate_examples
        # Users should dynamically create their own subsplits
        # with the subsplit API (e.g. split="train[80%:]")
        # See: https://github.com/tensorflow/datasets/blob/master/docs/splits.md
        return [
            sq.core.SplitGenerator(
                name=sq.Split.TRAIN,
                gen_kwargs={
                    "path": dl_path,
                },
            )
        ]

    def _generate_examples(self, path):
        """Yields examples."""
        f = sq.core.lazy_imports.h5py.File(path, "r")
        class_names = [x.astype(str) for x in f["note_1"]["data"][()]]
        # Assert that the class names are correct
        try:
            assert _CLASS_NAMES == class_names
        except AssertionError as e:
            print(e, """\n Assertion of class names failed. Provided by the routine: {}
            , but extracted from file: {}""".format(_CLASS_NAMES, class_names))
            raise e
        except Exception as e:
            print(e, """\n Exception happened when asserting class names. Provided by the routine: {}
            , but extracted from file: {}""".format(_CLASS_NAMES, class_names))
            raise e
        index = list(f)[1:-1]
        for entry in index:
            photon_energy = f["/{}/instrument_1/source_1/energy".format(entry)][()].astype(np.float32)  # in J
            wavelength = np.array(
                cs.h * cs.c / photon_energy * 1e9).astype(np.float32)  # in nm
            photon_energy /= _EV  # to eV
            detector_distance = np.array(
                f["/{}/instrument_1/detector_1/distance".format(entry)][()]).astype(
                np.float32) * 1e3  # in cm
            bunch_id = f["/{}/experimental_identifier".format(entry)][()].astype(np.int64)
            label = np.array(
                f["/{}/note_1/data".format(entry)][()]).flat.__array__().astype(np.int64)
            image = f["/{}/instrument_1/detector_1/data".format(entry)][()].astype(np.uint16)

            try:
                assert image.shape == _IMG_SHAPE
            except AssertionError as e:
                print(e, """\n Assertion of image shape failed. Provided by the routine: {}
                , but extracted from file: {}""".format(_IMG_SHAPE, image.shape))
                raise e
            except Exception as e:
                print(e, """\n Exception happened when asserting image shapes. Provided by the routine: {}
                , but extracted from file: {}""".format(_CLASS_NAMES, class_names))
                raise e

            record = {"photon_energy": np.float32(photon_energy),
                      "wavelength": np.float32(wavelength),
                      "detector_distance": np.float32(detector_distance),
                      "bunch_id": np.int64(bunch_id),
                      "image": np.uint16(image),
                      "label": np.int64(label)}
            yield int(bunch_id), record
