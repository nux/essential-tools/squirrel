"""
Squirrel module of the balanced helium nanodroplets data from the 2015 experiment at the
FERMI FEL-1. The experimental details are described in
Phys. Rev. Lett. 121, 255301; Langbehn et al (2018). In addition to
the scattering data, the data contains labels for a supervised
machine learning task. These labels are subject of the publication
Phys. Rev. E 99, 063309; Zimmermann et al (2019) about the applicability
of neural networks within the domain of coherent diffraction imaging.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import scipy.constants as cs
import tensorflow as tf

import squirrel.public_api as sq

_CITATION = r"""@article{PhysRevE.99.063309,
title = {Deep neural networks for classifying complex features in diffraction images},
author = {Zimmermann, Julian and Langbehn, Bruno and Cucini, Riccardo and Di Fraia, Michele and Finetti, Paola and LaForge, Aaron C. and Nishiyama, Toshiyuki and Ovcharenko, Yevheniy and Piseri, Paolo and Plekan, Oksana and Prince, Kevin C. and Stienkemeier, Frank and Ueda, Kiyoshi and Callegari, Carlo and M\"oller, Thomas and Rupp, Daniela},
journal = {Phys. Rev. E},
volume = {99},
issue = {6},
pages = {063309},
numpages = {20},
year = {2019},
month = {Jun},
publisher = {American Physical Society},
doi = {10.1103/PhysRevE.99.063309},
url = {https://link.aps.org/doi/10.1103/PhysRevE.99.063309}
}
"""

_DESCRIPTION = """
Squirrel module of the balanced static helium data from the 2015 experiment at the LDM
end station at FERMI FEL-1. The experimental details are described in
Phys. Rev. Lett. 121, 255301; Langbehn et al (2018). In addition to
the scattering data, the data file contains labels for a supervised
machine learning task.

This is the balanced sparse one-hot variant of the full dataset.
See section 4.6.3 in 

"Probing ultrafast electron dynamics in helium nanodroplets with 
deep learning assisted diffraction imaging"
Dissertation | Julian Zimmermann

The classes are:
1: Round
2: Elliptical
3: Asymmetric
4: Streak
5: Bent
6: DoubleRings

Further, the images are scaled between 0 and 1 and have a resolution of 256 x 256.
Radius is in nm.
"""

_URL = "https://share.phys.ethz.ch/~nux/datasets/balanced_helium_nanodroplets_with_labels_zimmermann.npz"

_CLASS_NAMES = [
    "Round", "Elliptical", "Asymmetric", "Streak", "Bent", "DoubleRings"
]

_IMG_SHAPE = (256, 256, 1)


class BalancedStaticHeliumNanodroplets(sq.core.GeneratorBasedBuilder):
    """
    Squirrel module of the balanced helium nanodroplets data from the 2015 experiment
    at the FERMI FEL-1. The experimental details are described in
    Phys. Rev. Lett. 121, 255301; Langbehn et al (2018). In addition to
    the scattering data, the data contains labels for a supervised
    machine learning task. These labels are subject of the publication
    Phys. Rev. E 99, 063309; Zimmermann et al (2019) about the applicability
    of neural networks within the domain of coherent diffraction imaging.
    """

    VERSION = sq.core.Version("0.1.0")

    def _info(self):
        return sq.core.DatasetInfo(
            builder=self,
            # This is the description that will appear on the datasets page.
            description=_DESCRIPTION,
            # sq.features.FeatureConnectors
            features=sq.features.FeaturesDict({
                "radius":
                    sq.features.Tensor(dtype=tf.int64,
                                       shape=[]),
                "image":
                    sq.features.Tensor(dtype=tf.float64,
                                       shape=_IMG_SHAPE),
                "label":
                    sq.features.ClassLabel(names=_CLASS_NAMES),
            }),
            # If there"s a common (input, target) tuple from the features,
            # specify them here. They"ll be used if as_supervised=True in
            # builder.as_dataset.
            supervised_keys=("image", "label"),
            # Homepage of the dataset for documentation
            homepage="https://www.cxidb.org/id-94.html",
            # Bibtex citation for the dataset
            citation=_CITATION,
        )

    def _split_generators(self, dl_manager):
        """Returns SplitGenerators."""
        # Download source data
        dl_path = dl_manager.download(_URL)

        # gen_kwargs will be passed as arguments to _generate_examples
        # Users should dynamically create their own subsplits
        # with the subsplit API (e.g. split="train[80%:]")
        # See: https://github.com/tensorflow/datasets/blob/master/docs/splits.md
        return [
            sq.core.SplitGenerator(
                name=sq.Split.TRAIN,
                gen_kwargs={
                    "path": dl_path,
                },
            )
        ]

    def _generate_examples(self, path):
        """Yields examples."""
        data = np.load(path)
        keys = list(data.keys())
        for ii, (i, l, r) in enumerate(zip(data[keys[0]], data[keys[1]], data[keys[2]])):
            try:
                assert i.shape == _IMG_SHAPE
            except AssertionError as e:
                print(e, """\n Assertion of image shape failed. Provided by the routine: {}
                , but extracted from file: {}""".format(_IMG_SHAPE, i.shape))
                raise e

            if np.isnan(r):
                rad = 0
            else:
                rad = int(r)

            record = {"radius": rad, "image": i, "label": l}
            yield ii, record
