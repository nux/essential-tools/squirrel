# coding=utf-8
# Copyright 2021 The TensorFlow Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Testing utilities."""

from squirrel.testing.dataset_builder_testing import DatasetBuilderTestCase
from squirrel.testing.feature_test_case import FeatureExpectationItem
from squirrel.testing.feature_test_case import FeatureExpectationsTestCase
from squirrel.testing.feature_test_case import RaggedConstant
from squirrel.testing.feature_test_case import SubTestCase
from squirrel.testing.mocking import mock_data
from squirrel.testing.mocking import MockPolicy
from squirrel.testing.test_case import TestCase
from squirrel.testing.test_utils import assert_features_equal
from squirrel.testing.test_utils import DummyBeamDataset
from squirrel.testing.test_utils import DummyDataset
from squirrel.testing.test_utils import DummyDatasetSharedGenerator
from squirrel.testing.test_utils import DummyMnist
from squirrel.testing.test_utils import DummyParser
from squirrel.testing.test_utils import DummySerializer
from squirrel.testing.test_utils import fake_examples_dir
from squirrel.testing.test_utils import make_tmp_dir
from squirrel.testing.test_utils import mock_kaggle_api
from squirrel.testing.test_utils import mock_tf
from squirrel.testing.test_utils import MockFs
from squirrel.testing.test_utils import rm_tmp_dir
from squirrel.testing.test_utils import run_in_graph_and_eager_modes
from squirrel.testing.test_utils import test_main
from squirrel.testing.test_utils import tmp_dir

__all__ = [
    "assert_features_equal",
    "DummyDataset",
    "DatasetBuilderTestCase",
    "DummyDatasetSharedGenerator",
    "DummyMnist",
    "fake_examples_dir",
    "FeatureExpectationItem",
    "FeatureExpectationsTestCase",
    "SubTestCase",
    "TestCase",
    "RaggedConstant",
    "run_in_graph_and_eager_modes",
    "test_main",
    "tmp_dir",  # TODO(afrozm): rm from here and add as methods to TestCase
    "make_tmp_dir",  # TODO(afrozm): rm from here and add as methods to TestCase
    "mock_kaggle_api",
    "mock_data",
    "MockFs",
    "MockPolicy",
    "rm_tmp_dir",  # TODO(afrozm): rm from here and add as methods to TestCase
]
