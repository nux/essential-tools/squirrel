# coding=utf-8
# Copyright 2021 The TensorFlow Datasets Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Structured datasets."""

from squirrel.structured.amazon_us_reviews import AmazonUSReviews
from squirrel.structured.cherry_blossoms import CherryBlossoms
from squirrel.structured.covid19 import Covid19
from squirrel.structured.dart import Dart
from squirrel.structured.e2e_cleaned import E2eCleaned
from squirrel.structured.efron_morris_75 import EfronMorris75
from squirrel.structured.forest_fires import ForestFires
from squirrel.structured.genomics_ood import GenomicsOod
from squirrel.structured.german_credit_numeric import GermanCreditNumeric
from squirrel.structured.higgs import Higgs
from squirrel.structured.howell import Howell
from squirrel.structured.iris import Iris
from squirrel.structured.kddcup99 import Kddcup99
from squirrel.structured.movielens import Movielens
from squirrel.structured.penguins import Penguins
from squirrel.structured.proteinnet import ProteinNet
from squirrel.structured.radon import Radon
from squirrel.structured.rock_you import RockYou
from squirrel.structured.titanic import Titanic
from squirrel.structured.web_nlg.web_nlg import WebNlg
from squirrel.structured.wiki_bio import WikiBio
from squirrel.structured.wiki_table_questions import WikiTableQuestions
from squirrel.structured.wiki_table_text import WikiTableText
from squirrel.structured.wine_quality import WineQuality
