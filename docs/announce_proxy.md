# Introducing TensorFlow Datasets

TensorFlow Datasets is now released on PyPI:

`pip install squirrel`

Read the [blog post](https://medium.com/tensorflow/introducing-squirrel-c7f01f7e19f3)
to learn more.
