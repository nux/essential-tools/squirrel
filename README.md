# Squirrel

Squirrel is a library of scientific datasets ready to use with TensorFlow.
It is based on a modified fork of [Tensorflow Datasets](https://www.tensorflow.org/datasets)

#### List of available datasets:
* Supervised:
  1. Everything that is within tensorflow/datasets (see ```sq.list_builders()```)
  2. [Static Helium Nanodroplets](squirrel/docs/catalog/static_helium_nanodroplets.md) | Publication: [Zimmermann, J. et al. Phys. Rev. E 99, 063309 (2019)](https://link.aps.org/doi/10.1103/PhysRevE.99.063309)
  2. Balanced Static Helium Nanodroplets | Publication: [Zimmermann, J. et al. Phys. Rev. E 99, 063309 (2019)](https://link.aps.org/doi/10.1103/PhysRevE.99.063309)
* Unsupervised:
  1. Dynamic Helium Nanodroplets (coming soon)
* Simulated:
  1. Large regression dataset with 1.3M images. Created using PyScatman (Name="simulated_helium_nanodroplets_large")
  2. Real-time MSFT simulations (coming soon)

#### How to use:
```python
# Importing the static Helium nanodroplets data
# Splitting can be dynamically adjusted using the TF split API:
# https://github.com/tensorflow/datasets/blob/master/docs/splits.md
# Here: Use 10% for testing and the rest for training
import squirrel as sq
(test, train), info = sq.load(name="static_helium_nanodroplets", 
                            split=["train[:10%]", "train[10%:]"], 
                            with_info=True)
# Build your input pipeline
ds_train = train.shuffle(1000).batch(128).prefetch(10)
for features in ds_train.take(1):
  image, label = features["image"], features["label"]
# Done
```

#### How to install:
```shell
# Clone the repo
git clone git@gitlab.ethz.ch:nux/essential-tools/squirrel.git

# cd into the main directory
cd squirrel

# Build pip wheel
python setup.py bdist_wheel

# Install module with pip
pip install dist/squirrel-0.2.0-py3-none-any.whl
```

#### Further information
##### The `DatasetBuilder` class

Please see also the [Tensorflow Datasets](https://www.tensorflow.org/datasets) API as Squirrel is based on it.

All datasets are implemented as subclasses of `sq.core.DatasetBuilder`. Squirrel
has two entry points:

*   `sq.builder`: Returns the `sq.core.DatasetBuilder` instance, giving
    control over `builder.download_and_prepare()` and
    `builder.as_dataset()`.
*   `sq.load`: Convenience wrapper which hides the `download_and_prepare` and
    `as_dataset` calls, and directly returns the `tf.data.Dataset`.

```python
import squirrel as sq

# The following is the equivalent of the `load` call above.

# You can fetch the DatasetBuilder class by string
mnist_builder = sq.builder("mnist")

# Download the dataset
mnist_builder.download_and_prepare()

# Construct a tf.data.Dataset
ds = mnist_builder.as_dataset(split="train")

# Get the `DatasetInfo` object, which contains useful information about the
# dataset and its features
info = mnist_builder.info
print(info)
```

This will print the dataset info content:

```python
sq.core.DatasetInfo(
    name="mnist",
    version=3.0.1,
    description="The MNIST database of handwritten digits.",
    homepage="http://yann.lecun.com/exdb/mnist/",
    features=FeaturesDict({
        "image": Image(shape=(28, 28, 1), dtype=tf.uint8),
        "label": ClassLabel(shape=(), dtype=tf.int64, num_classes=10),
    }),
    total_num_examples=70000,
    splits={
        "test": 10000,
        "train": 60000,
    },
    supervised_keys=("image", "label"),
    citation="""@article{lecun2010mnist,
      title={MNIST handwritten digit database},
      author={LeCun, Yann and Cortes, Corinna and Burges, CJ},
      journal={ATT Labs [Online]. Available: http://yann.lecun.com/exdb/mnist},
      volume={2},
      year={2010}
    }""",
    redistribution_info=,
)
```

You can also get details about the classes (number of classes and their names).

```python
info = sq.builder("cats_vs_dogs").info

info.features["label"].num_classes  # 2
info.features["label"].names  # ["cat", "dog"]
info.features["label"].int2str(1)  # "dog"
info.features["label"].str2int("cat")  # 0
```

##### NumPy Usage with `sq.as_numpy`

As a convenience for users that want simple NumPy arrays in their programs, you
can use `sq.as_numpy` to return a generator that yields NumPy array
records out of a `tf.data.Dataset`. This allows you to build high-performance
input pipelines with `tf.data` but use whatever you"d like for your model
components.

```python
train_ds = sq.load("mnist", split="train")
train_ds = train_ds.shuffle(1024).batch(128).repeat(5).prefetch(10)
for example in sq.as_numpy(train_ds):
  numpy_images, numpy_labels = example["image"], example["label"]
```

You can also use `sq.as_numpy` in conjunction with `batch_size=-1` to
get the full dataset in NumPy arrays from the returned `tf.Tensor` object:

```python
train_ds = sq.load("mnist", split=sq.Split.TRAIN, batch_size=-1)
numpy_ds = sq.as_numpy(train_ds)
numpy_images, numpy_labels = numpy_ds["image"], numpy_ds["label"]
```

Note that the library still requires `tensorflow` as an internal dependency.

##### *Disclaimers*

*This is a utility library that downloads and prepares public datasets. We do*
*not host or distribute these datasets, vouch for their quality or fairness, or*
*claim that you have license to use the dataset. It is your responsibility to*
*determine whether you have permission to use the dataset under the dataset"s*
*license.*

*If you"re a dataset owner and wish to update any part of it (description,*
*citation, etc.), or do not want your dataset to be included in this*
*library, please get in touch through a GitHub issue. Thanks for your*
*contribution to the ML community!*

*If you"re interested in learning more about responsible AI practices, including*
*fairness, please see Google AI"s [Responsible AI Practices](https://ai.google/education/responsible-ai-practices).*

*`Squirrel` is Apache 2.0 licensed. See the
[`LICENSE`](LICENSE) file.*
